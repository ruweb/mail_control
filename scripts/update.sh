#!/bin/sh
exec 2>&1
export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"

chmod -vvR go+r /home/*/.plugins/mail_control

grep -q '/plugins/Mail_Control/plugin.conf' /usr/local/directadmin/scripts/custom/domain_create_post.sh || echo '
if [ -e /usr/local/directadmin/plugins/Mail_Control/plugin.conf ]; then
    for dir in "/home/$username/.plugins" "/home/$username/.plugins/mail_control"; do
        [ -L "$dir" ] && exit 0
        [ -d "$dir" ] || (mkdir "$dir" && chown -h "$username:$username" "$dir" && chmod -h 755 "$dir")
    done
    [ -L "$dir/from_addresses" ] && exit 0
    [ ! -e "$dir/from_addresses" ] && touch "$dir/from_addresses" && chown -h "$user:$user" "$dir/from_addresses" && chmod -h 644 "$dir/from_addresses"
    grep -q "^\*@$domain\$" "$dir/from_addresses" || echo "*@$domain" >> "$dir/from_addresses"
fi' >> /usr/local/directadmin/scripts/custom/domain_create_post.sh

grep -q '/etc/virtual/$sender_address_domain/whitelist_senders' /etc/exim.conf || exit 0;

for domain in $(grep -o '^[^:]*' /etc/virtual/domainowners); do
    user=$(grep "^$domain:" /etc/virtual/domainowners | awk '{print $2}' | head -n 1)
    if [ ! -e "/etc/virtual/$domain/" ] && [ -d "/etc/virtual/${domain}_off/" ]; then
        dom="${domain}_off"
    else
        dom=$domain
    fi
    [ -z "$user" ] && continue
    [ -d "/home/$user" ] || continue
    grep -q "^$user:" /etc/passwd || continue
    for dir in "/home/$user/.plugins" "/home/$user/.plugins/mail_control"; do
        [ -d "$dir" ] || (mkdir "$dir" && chown -h "$user:$user" "$dir" && chmod -h 755 "$dir")
    done
    for flag in check_reverse greylist callout callout_off ignore_suspicious discard_bounces rbl_abuseat_off rbl_sorbs_off rbl_spamcop \
            whitelist_senders noreply_addresses from_addresses from_check_off; do
        if [ "$flag" = "from_check_off" ]; then
            [ ! -e "$dir/$flag" ] && touch "$dir/$flag" && chown -h "$user:$user" "$dir/$flag" && chmod -h 644 "$dir/$flag"
        elif [ "$flag" = "from_addresses" ]; then
            echo "*@$domain" >> "$dir/$flag" && chown -h "$user:$user" "$dir/$flag" && chmod -h 644 "$dir/$flag"
        elif [ "$flag" = "noreply_addresses" ]; then
            [ -e "/etc/virtual/$dom/$flag" ] && for addr in $(grep -o '[^[:space:]]*' "/etc/virtual/$dom/$flag"); do
                echo "$addr@$domain" >> "$dir/$flag" && chown -h "$user:$user" "$dir/$flag" && chmod -h 644 "$dir/$flag"
                echo "/etc/virtual/$dom/$flag ->> $dir/$flag"
            done
        elif [ -e "/etc/virtual/$dom/$flag" ]; then
            if [ "$flag" = "whitelist_senders" -o "$flag" = "noreply_addresses" ] && [ -e "$dir/$flag" ]; then
                grep -o '[^[:space:]]*' "/etc/virtual/$dom/$flag" | cat /dev/stdin "$dir/$flag" | sort -uo "$dir/$flag" 2>/dev/null
                echo "/etc/virtual/$dom/$flag >> $dir/$flag"
            else
                mv -v "/etc/virtual/$dom/$flag" "$dir/$flag"
            fi
            chown -h "$user:$user" "$dir/$flag" && chmod -h 644 "$dir/$flag"
        fi
    done
done

perl -ni -e 'unless (m/#Added by Mail Control Plugin$/) { print }' /etc/crontab 2>&1

/root/conf_update/conf_update.sh exim y

if [ -e /usr/local/etc/rc.d/clamav-clamd ]; then
    ! grep -q clamav_clamd_enable /etc/rc.conf && echo clamav_clamd_enable="YES" >> /etc/rc.conf \
        && echo "action=clamd&value=start" >> /usr/local/directadmin/data/task.queue && ln -s clamav-clamd /usr/local/etc/rc.d/clamd
    grep clamd /usr/local/directadmin/data/admin/services.status || echo clamd=ON >> /usr/local/directadmin/data/admin/services.status
fi

echo "Plugin has been updated!"; #NOT! :)

exit 0;
