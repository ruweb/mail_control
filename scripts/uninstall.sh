#!/bin/sh
exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"
set -x

perl -ni -e 'unless (m/#Added by Mail Control Plugin$/) { print }' /etc/crontab 2>&1

set +x
echo "Plugin Un-Installed!"; #NOT! :)

exit 0;
